package com.zhonkophe.producers.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zhonkophe.producers.viewmodel.FarmdropProducersViewModel
import com.zhonkophe.producers.util.viewmodelfactory.ViewModelFactory
import com.zhonkophe.producers.util.viewmodelfactory.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
  
  @Binds
  @IntoMap
  @ViewModelKey(FarmdropProducersViewModel::class)
  internal abstract fun bindRequestDetailsViewModell(viewModel: FarmdropProducersViewModel): ViewModel
  
  @Binds
  abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
