package com.zhonkophe.producers.di

import androidx.databinding.DataBindingComponent
import com.zhonkophe.producers.util.binding.CustomBindings

class BindingComponent : DataBindingComponent {

  override fun getCustomBindings(): CustomBindings = CustomBindings()

}