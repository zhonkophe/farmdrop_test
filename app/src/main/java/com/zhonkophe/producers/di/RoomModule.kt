package com.zhonkophe.producers.di

import android.content.Context
import androidx.room.Room
import com.zhonkophe.producers.datasource.cache.ProducersDatabase
import com.zhonkophe.producers.util.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RoomModule {

    @Provides
    @Singleton
    internal fun provideProducersDatabase(context: Context): ProducersDatabase =
        Room.databaseBuilder(
            context.applicationContext,
            ProducersDatabase::class.java,
            DATABASE_NAME
        ).build()

    @Provides
    @Singleton
    internal fun provideProducersDao(producersDatabase: ProducersDatabase) = producersDatabase.producersDao()
}