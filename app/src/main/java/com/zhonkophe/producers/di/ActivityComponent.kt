package com.zhonkophe.producers.di

import androidx.lifecycle.ViewModelProvider
import dagger.Subcomponent

@Subcomponent
interface ActivityComponent {

  fun viewFactory(): ViewModelProvider.Factory

}