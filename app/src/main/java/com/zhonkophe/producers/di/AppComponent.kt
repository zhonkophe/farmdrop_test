package com.zhonkophe.producers.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        DataModule::class,
        SchedulersModule::class,
        RoomModule::class
    ]
)
interface AppComponent {

    fun activityComponent(): ActivityComponent

}