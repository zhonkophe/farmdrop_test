package com.zhonkophe.producers.di

import android.content.Context
import com.zhonkophe.producers.BuildConfig
import com.zhonkophe.producers.datasource.api.FarmdropService
import com.zhonkophe.producers.util.BASE_URL
import com.zhonkophe.producers.util.connectivity.ConnectionStateMonitor
import com.zhonkophe.producers.util.connectivity.DefaultConnectionStateMonitor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideAuthService(retrofit: Retrofit): FarmdropService =
        retrofit.create(FarmdropService::class.java)

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().setLevel(
                    if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                    else HttpLoggingInterceptor.Level.NONE
                )
            )
            .build()

    @Provides
    @Singleton
    fun provideConnectionStateMonitor(context: Context): ConnectionStateMonitor = DefaultConnectionStateMonitor(context)
}
