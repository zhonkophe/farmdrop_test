package com.zhonkophe.producers.di

import com.zhonkophe.producers.datasource.api.FarmdropService
import com.zhonkophe.producers.datasource.cache.ProducersDao
import com.zhonkophe.producers.datasource.cache.ProducersDatabase
import com.zhonkophe.producers.repository.DefaultFarmdropRepository
import com.zhonkophe.producers.repository.FarmdropRepository
import com.zhonkophe.producers.util.connectivity.ConnectionStateMonitor
import com.zhonkophe.producers.util.schedulers.SchedulersProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    internal fun provideFarmdropProducersRepository(
        service: FarmdropService,
        producersDao: ProducersDao,
        connectionStateMonitor: ConnectionStateMonitor,
        schedulersProvider: SchedulersProvider
    ): FarmdropRepository =
        DefaultFarmdropRepository(service, producersDao, connectionStateMonitor, schedulersProvider)

}
