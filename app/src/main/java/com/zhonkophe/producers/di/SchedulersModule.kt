package com.zhonkophe.producers.di

import com.zhonkophe.producers.util.schedulers.DefaultSchedulersProvider
import com.zhonkophe.producers.util.schedulers.SchedulersProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class SchedulersModule {
  @Provides @Singleton internal open fun provideSchedulersProvider(): SchedulersProvider =
      DefaultSchedulersProvider()
}