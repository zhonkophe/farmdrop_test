package com.zhonkophe.producers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import com.google.android.material.snackbar.Snackbar
import com.zhonkophe.producers.adapter.FarmdropProducersAdapter
import com.zhonkophe.producers.app.FarmdropApp
import com.zhonkophe.producers.databinding.ActivityMainBinding
import com.zhonkophe.producers.repository.FarmdropProducer
import com.zhonkophe.producers.repository.RequestStatus
import com.zhonkophe.producers.viewmodel.FarmdropProducersViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel: FarmdropProducersViewModel by lazy {
        val factory = (application as? FarmdropApp)?.appComponent?.activityComponent()?.viewFactory()
        ViewModelProviders.of(this, factory)
            .get(FarmdropProducersViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        setSupportActionBar(appToolbar)
        observeErrors()
        initAdapter()

        viewModel.start()
    }

    private fun initAdapter() {
        val adapter = FarmdropProducersAdapter()
        producers.adapter = adapter
        val lifecycleOwner = this as LifecycleOwner
        viewModel.listingMediator.observe(lifecycleOwner,
            Observer<PagedList<FarmdropProducer>>
            {
                adapter.submitList(it)
            })

    }

    private fun observeErrors() {
        viewModel.requestStatus.observe(this, Observer {
            if (it is RequestStatus.StatusFailed) showErrorMessage()
        })
    }

    private fun showErrorMessage() {
        Snackbar.make(rootView, resources.getString(R.string.error_message), Snackbar.LENGTH_LONG)
            .setAction(resources.getString(R.string.retry)) { viewModel.retry() }
            .show()
    }
}
