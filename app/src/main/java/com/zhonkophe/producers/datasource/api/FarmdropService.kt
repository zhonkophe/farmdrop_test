package com.zhonkophe.producers.datasource.api

import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface FarmdropService {

    @GET("/2/producers/")
    fun getFarmdropProducers(
        @Query("page") page: Int,
        @Query("per_page_limit") limit: Int
    ): Flowable<FarmdropProducersResponse>

}
