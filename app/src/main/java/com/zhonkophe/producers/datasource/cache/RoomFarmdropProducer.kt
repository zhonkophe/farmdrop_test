package com.zhonkophe.producers.datasource.cache

import androidx.paging.DataSource
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Entity(tableName = "producers")
data class RoomFarmdropProducer(
    @ColumnInfo(name = "id") @PrimaryKey val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "short_description") val shortDescription: String,
    @ColumnInfo(name = "image_url") val imageUrl: String?
)

@Dao
interface ProducersDao {

    @Query("SELECT * FROM producers")
    fun selectPaged(): DataSource.Factory<Int, RoomFarmdropProducer>

    @Insert(onConflict = REPLACE)
    fun insertProducers(producers: List<RoomFarmdropProducer>)

    @Query("DELETE from producers")
    fun deleteAll()
}

@Database(entities = [RoomFarmdropProducer::class], version = 1)
abstract class ProducersDatabase : RoomDatabase() {

    abstract fun producersDao(): ProducersDao

}