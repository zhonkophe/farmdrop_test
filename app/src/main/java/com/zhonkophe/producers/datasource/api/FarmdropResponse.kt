package com.zhonkophe.producers.datasource.api

import com.google.gson.annotations.SerializedName

class FarmdropProducersResponse {
    @SerializedName("response") val response: List<FarmdropProducerItemResponse>? = null
}

class FarmdropProducerItemResponse {
    @SerializedName("name") val name: String? = null
    @SerializedName("id") val id: String? = null
    @SerializedName("images") val images: List<FarmdropProducerItemImageResponse>? = emptyList()
    @SerializedName("short_description") val shortDescription: String? = null
}

class FarmdropProducerItemImageResponse {
    @SerializedName("path") val path: String? = null
}