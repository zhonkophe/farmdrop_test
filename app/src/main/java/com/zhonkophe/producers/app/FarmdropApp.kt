package com.zhonkophe.producers.app

import android.app.Application
import androidx.databinding.DataBindingUtil
import com.zhonkophe.producers.di.*

class FarmdropApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        setupAppComponent()
        setupDataBindingComponent()
    }

    private fun setupAppComponent() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .roomModule(RoomModule())
            .schedulersModule(SchedulersModule())
            .networkModule(NetworkModule())
            .build()
    }

    private fun setupDataBindingComponent() {
        DataBindingUtil.setDefaultComponent(BindingComponent())
    }

}