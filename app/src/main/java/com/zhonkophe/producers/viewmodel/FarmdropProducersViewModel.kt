package com.zhonkophe.producers.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.zhonkophe.producers.repository.DataSourceStatus
import com.zhonkophe.producers.repository.FarmdropProducer
import com.zhonkophe.producers.repository.FarmdropRepository
import com.zhonkophe.producers.repository.RequestStatus
import javax.inject.Inject

class FarmdropProducersViewModel @Inject constructor(
    private val repository: FarmdropRepository
) : ViewModel() {

    val requestStatus = MutableLiveData<RequestStatus>()
    val dataSource = MutableLiveData<DataSourceStatus>()
    val showRefreshIndicator = MutableLiveData<Boolean>()
    val nothingToShow = MutableLiveData<Boolean>()
    val listingMediator = MediatorLiveData<PagedList<FarmdropProducer>>()

    private var started = false

    fun start() {
        if (started) return
        started = true
        requestInfo()
    }

    fun refresh() {
        repository.reload()
        showRefreshIndicator.postValue(false)
    }

    fun retry() {
        repository.retry()
    }

    private fun requestInfo() {
        val listingValue = repository.getProducersPagedList()
        listingMediator.addSource(listingValue.requestStatus) { requestStatus.postValue(it) }
        listingMediator.addSource(listingValue.pagedList) {
            listingMediator.postValue(it)
            nothingToShow.postValue(it.isNullOrEmpty())
        }
        listingMediator.addSource(listingValue.dataSource) { dataSource.postValue(it) }
    }

}
