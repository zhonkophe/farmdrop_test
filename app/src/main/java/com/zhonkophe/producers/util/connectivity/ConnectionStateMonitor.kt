package com.zhonkophe.producers.util.connectivity

import io.reactivex.Flowable

interface ConnectionStateMonitor {

    fun connectionAvailability(): Flowable<Boolean>

    fun isNetworkAvailable(): Boolean

}