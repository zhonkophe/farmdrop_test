package com.zhonkophe.producers.util

const val ITEMS_PER_PAGE = 20
const val INITIAL_PAGE = 1
const val PREFETCH_DISTANCE = 5

const val DATABASE_NAME = "ProducersDB"

const val BASE_URL = "https://api.farmdrop.com/2/"
