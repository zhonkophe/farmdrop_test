package com.zhonkophe.producers.util.connectivity

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject

class DefaultConnectionStateMonitor(context: Context) : NetworkCallback(), ConnectionStateMonitor {

    private val connectionAvailability = BehaviorSubject.create<Boolean>()
    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private val networkRequest: NetworkRequest =
        NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()

    init {
        enable()
    }

    override fun isNetworkAvailable(): Boolean {
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork?.isConnected == true
    }

    override fun connectionAvailability(): Flowable<Boolean> =
        connectionAvailability.toFlowable(BackpressureStrategy.LATEST)

    private fun enable() {
        connectivityManager.registerNetworkCallback(networkRequest, this)
    }

    // Likewise, you can have a disable method that simply calls ConnectivityManager.unregisterNetworkCallback(NetworkCallback) too.

    override fun onAvailable(network: Network) {
        super.onAvailable(network)
        connectionAvailability.onNext(true)
    }

    override fun onUnavailable() {
        super.onUnavailable()
        connectionAvailability.onNext(false)
    }
}