package com.zhonkophe.producers.util.binding

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter
import com.zhonkophe.producers.R
import com.zhonkophe.producers.repository.DataSourceStatus
import com.zhonkophe.producers.repository.RequestStatus

class CustomBindings {

    @BindingAdapter("loadingVisibility")
    fun View.setRequestStatus(requestStatus: RequestStatus?) {
        this.visibility = if (requestStatus is RequestStatus.StatusInProgress) VISIBLE else GONE
    }

    @BindingAdapter("dataSourceStatus")
    fun Toolbar.setDataSourceStatus(dataSourceStatus: DataSourceStatus?) {
        this.subtitle = resources.getString(
            when (dataSourceStatus) {
                is DataSourceStatus.Cache -> R.string.showing_cached_results
                is DataSourceStatus.Api -> R.string.showing_api_results
                else -> R.string.undkown_status
            }
        )
    }

    @BindingAdapter("dataSourceBackgroundStatus")
    fun View.setDataSourceBackgroundStatus(dataSourceStatus: DataSourceStatus?) {
        this.setBackgroundColor(
            resources.getColor(
                when (dataSourceStatus) {
                    is DataSourceStatus.Cache -> R.color.colorDisabled
                    is DataSourceStatus.Api -> R.color.colorPrimary
                    else -> R.color.colorPrimary
                }
            )
        )
    }

}