package com.zhonkophe.producers.util.extentions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.zhonkophe.producers.R

fun ImageView.setImageUrl(url: String?) {
    if (url == null) return
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.ic_fd_logo_with_title)
        .error(R.drawable.ic_fd_logo_with_title)
        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        .into(this)
}