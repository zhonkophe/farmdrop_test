package com.zhonkophe.producers.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zhonkophe.producers.R
import com.zhonkophe.producers.repository.FarmdropProducer
import com.zhonkophe.producers.util.extentions.setImageUrl

class FarmdropProducerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val title = view.findViewById<TextView>(R.id.itemTitle)
    private val description = view.findViewById<TextView>(R.id.itemDescription)
    private val image = view.findViewById<ImageView>(R.id.itemImage)

    fun bind(item: FarmdropProducer?) {
        if (item == null) return
        title.text = item.name
        description.text = item.shorDescription
        image.setImageUrl(item.imageUrl)
    }

    companion object {
        fun create(parent: ViewGroup)
                : FarmdropProducerViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row, parent, false)
            return FarmdropProducerViewHolder(view)
        }
    }

}
