package com.zhonkophe.producers.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.zhonkophe.producers.repository.FarmdropProducer

class FarmdropProducersAdapter : PagedListAdapter<FarmdropProducer, FarmdropProducerViewHolder>(POST_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FarmdropProducerViewHolder {
        return FarmdropProducerViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: FarmdropProducerViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<FarmdropProducer>() {

            override fun areContentsTheSame(oldItem: FarmdropProducer, newItem: FarmdropProducer): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(oldItem: FarmdropProducer, newItem: FarmdropProducer): Boolean =
                oldItem.id == newItem.id && oldItem.name == newItem.name

        }
    }

}
