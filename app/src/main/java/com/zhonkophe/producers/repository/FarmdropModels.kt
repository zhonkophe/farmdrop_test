package com.zhonkophe.producers.repository

data class FarmdropProducer(val name: String, val shorDescription: String, val imageUrl: String? = null, val id: String)