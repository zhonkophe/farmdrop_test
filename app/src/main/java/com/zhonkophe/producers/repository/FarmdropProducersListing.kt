package com.zhonkophe.producers.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class FarmdropProducersListing(
    val pagedList: LiveData<PagedList<FarmdropProducer>>,
    val requestStatus: LiveData<RequestStatus>,
    val dataSource: LiveData<DataSourceStatus>
)

sealed class RequestStatus {
    object StatusInProgress : RequestStatus()
    object StatusFinished : RequestStatus()
    object StatusFailed : RequestStatus()
}

sealed class DataSourceStatus {
    object Api : DataSourceStatus()
    object Cache : DataSourceStatus()
}

