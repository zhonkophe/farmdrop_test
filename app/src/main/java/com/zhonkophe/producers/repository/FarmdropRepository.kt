package com.zhonkophe.producers.repository

interface FarmdropRepository {

    fun getProducersPagedList(): FarmdropProducersListing

    fun retry()

    fun reload()

}