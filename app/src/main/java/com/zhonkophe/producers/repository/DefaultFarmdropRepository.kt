package com.zhonkophe.producers.repository

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.zhonkophe.producers.datasource.api.FarmdropProducerItemResponse
import com.zhonkophe.producers.datasource.api.FarmdropService
import com.zhonkophe.producers.datasource.cache.ProducersDao
import com.zhonkophe.producers.datasource.cache.RoomFarmdropProducer
import com.zhonkophe.producers.util.INITIAL_PAGE
import com.zhonkophe.producers.util.ITEMS_PER_PAGE
import com.zhonkophe.producers.util.PREFETCH_DISTANCE
import com.zhonkophe.producers.util.connectivity.ConnectionStateMonitor
import com.zhonkophe.producers.util.schedulers.SchedulersProvider
import io.reactivex.Flowable

class DefaultFarmdropRepository(
    private val farmdropService: FarmdropService,
    private val producersDao: ProducersDao,
    private val connectionStateMonitor: ConnectionStateMonitor,
    private val schedulersProvider: SchedulersProvider
) : FarmdropRepository {

    private var page = INITIAL_PAGE
    private val requestStatus = MutableLiveData<RequestStatus>()
    private val dataSourceStatus = MutableLiveData<DataSourceStatus>()
    private var retryData: RetryData? = null

    private val pagedListLiveData by lazy {
        val dataSourceFactory = producersDao.selectPaged().map { roomItemToFarmdropProducer(it) }
        val config = PagedList.Config.Builder()
            .setPageSize(ITEMS_PER_PAGE)
            .setPrefetchDistance(PREFETCH_DISTANCE)
            .build()
        LivePagedListBuilder(dataSourceFactory, config)
            .setBoundaryCallback(boundaryCallback)
            .build()
            .also { requestInitialPortion() }
    }

    private val boundaryCallback = object : PagedList.BoundaryCallback<FarmdropProducer>() {
        override fun onItemAtEndLoaded(itemAtFront: FarmdropProducer) {
            super.onItemAtFrontLoaded(itemAtFront)
            getProducers(
                ++page,
                {
                    dataSourceStatus.postValue(DataSourceStatus.Api)
                    cacheProducers(it)
                },
                { dataSourceStatus.postValue(DataSourceStatus.Cache) }
            )
        }
    }

    init {
        subscribeToNetworkState()
    }

    override fun getProducersPagedList(): FarmdropProducersListing {
        return FarmdropProducersListing(pagedListLiveData, requestStatus, dataSourceStatus)
    }

    override fun retry() {
        retryData?.let {
            getProducers(
                it.pageToRetry,
                { result ->
                    it.onSuccessAction.invoke(result)
                    retryData = null
                },
                it.onErrorAction
            )
        }
    }

    override fun reload() {
        retryData = null
        requestInitialPortion()
    }

    private fun requestInitialPortion() {
        page = 1
        dataSourceStatus.postValue(DataSourceStatus.Cache)
        getProducers(page, {
            producersDao.deleteAll()
            cacheProducers(it)
            dataSourceStatus.postValue(DataSourceStatus.Api)
        }, { })
    }

    @SuppressLint("CheckResult")
    private fun subscribeToNetworkState() {
        connectionStateMonitor.connectionAvailability()
            .subscribe { networkAvailable -> if (networkAvailable && retryData != null) retry() }
    }

    private fun cacheProducers(producers: List<FarmdropProducer>?) {
        producersDao.insertProducers(producers?.map { farmdropProducerToRoomItem(it) } ?: emptyList())
    }

    @SuppressLint("CheckResult")
    private fun getProducers(page: Int, success: (List<FarmdropProducer>?) -> Unit, error: (Throwable?) -> Unit) {
        if (!connectionStateMonitor.isNetworkAvailable()) {
            retryData = RetryData(page, success, error)
            requestStatus.postValue(RequestStatus.StatusFailed)
            error.invoke(null)
            return
        }
        farmdropService.getFarmdropProducers(page, ITEMS_PER_PAGE)
            .doOnSubscribe { requestStatus.postValue(RequestStatus.StatusInProgress) }
            .flatMap {
                Flowable.just(it.response?.mapIndexed { index, item -> apiItemToFarmdropProducer(item, index) }
                    ?: emptyList())
            }
            .subscribeOn(schedulersProvider.singleScheduler)
            .subscribe(
                {
                    requestStatus.postValue(RequestStatus.StatusFinished)
                    success.invoke(it)
                },
                {
                    requestStatus.postValue(RequestStatus.StatusFailed)
                    retryData = RetryData(page, success, error)
                    error.invoke(it)
                })
    }

    private fun roomItemToFarmdropProducer(item: RoomFarmdropProducer) =
        FarmdropProducer(
            id = item.id,
            name = item.name,
            shorDescription = item.shortDescription,
            imageUrl = item.imageUrl
        )

    private fun apiItemToFarmdropProducer(item: FarmdropProducerItemResponse, index: Int) =
        FarmdropProducer(
            name = item.name ?: "",
            imageUrl = item.images?.get(0)?.path,
            shorDescription = item.shortDescription ?: "",
            id = item.id ?: "$index"
        )

    private fun farmdropProducerToRoomItem(item: FarmdropProducer) =
        RoomFarmdropProducer(
            id = item.id,
            name = item.name,
            imageUrl = item.imageUrl,
            shortDescription = item.shorDescription
        )

}

private data class RetryData(
    val pageToRetry: Int,
    val onSuccessAction: (List<FarmdropProducer>?) -> Unit,
    val onErrorAction: (Throwable?) -> Unit
)
