package com.zhonkophe.producers.di

import com.zhonkophe.producers.ViewModelTest
import com.zhonkophe.producers.repository.FarmdropRepository
import com.zhonkophe.producers.viewmodel.FarmdropProducersViewModel
import dagger.Component
import dagger.Module
import dagger.Provides

@Component(
    modules = [TestViewModelModule::class]
)
interface TestViewComponent {

    fun inject(test: ViewModelTest)

}

@Module
class TestViewModelModule(private val repository: FarmdropRepository) {

    @Provides
    fun provideViewModel() = FarmdropProducersViewModel(repository)

}