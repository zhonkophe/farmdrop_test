package com.zhonkophe.producers.di

import com.zhonkophe.producers.util.schedulers.SchedulersProvider
import com.zhonkophe.producers.utils.TestSchedulersProvider
import dagger.Module
import dagger.Provides

@Module
class TestSchedulersModule {

    @Provides
    fun provideSchedulersProvider(): SchedulersProvider = TestSchedulersProvider()

}
