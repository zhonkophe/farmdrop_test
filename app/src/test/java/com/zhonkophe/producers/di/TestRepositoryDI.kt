package com.zhonkophe.producers.di

import com.zhonkophe.producers.RepositoryTest
import com.zhonkophe.producers.datasource.api.FarmdropService
import com.zhonkophe.producers.datasource.cache.ProducersDao
import com.zhonkophe.producers.repository.DefaultFarmdropRepository
import com.zhonkophe.producers.repository.FarmdropRepository
import com.zhonkophe.producers.util.connectivity.ConnectionStateMonitor
import com.zhonkophe.producers.util.schedulers.SchedulersProvider
import dagger.Component
import dagger.Module
import dagger.Provides

@Component(
    modules = [TestSchedulersModule::class,
        TestProducersRepositoryModule::class]
)
interface TestRepositoryComponent {

    fun inject(test: RepositoryTest)

}

@Module
class TestProducersRepositoryModule(
    private val farmdropService: FarmdropService,
    private val producersDao: ProducersDao,
    private val connectionStateMonitor: ConnectionStateMonitor
) {

    @Provides
    fun provideRepository(schedulersProvider: SchedulersProvider): FarmdropRepository =
        DefaultFarmdropRepository(farmdropService, producersDao, connectionStateMonitor, schedulersProvider)

}

