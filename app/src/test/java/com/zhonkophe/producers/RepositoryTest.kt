package com.zhonkophe.producers

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.zhonkophe.producers.datasource.api.FarmdropProducersResponse
import com.zhonkophe.producers.datasource.api.FarmdropService
import com.zhonkophe.producers.datasource.cache.ProducersDao
import com.zhonkophe.producers.di.DaggerTestRepositoryComponent
import com.zhonkophe.producers.di.TestProducersRepositoryModule
import com.zhonkophe.producers.di.TestSchedulersModule
import com.zhonkophe.producers.repository.DataSourceStatus
import com.zhonkophe.producers.repository.FarmdropRepository
import com.zhonkophe.producers.repository.RequestStatus
import com.zhonkophe.producers.util.schedulers.SchedulersProvider
import com.zhonkophe.producers.utils.TestConnectionStateMonitor
import com.zhonkophe.producers.utils.TestRoomDataSource
import com.zhonkophe.producers.utils.TestRoomFactory
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import javax.inject.Inject

@RunWith(JUnit4::class)
class RepositoryTest {

    @Rule @JvmField var mockitoRule = MockitoJUnit.rule()
    @Rule @JvmField var instantExecutorRule = InstantTaskExecutorRule()

    @Mock lateinit var farmdropService: FarmdropService
    @Mock lateinit var producersDao: ProducersDao

    @Inject lateinit var schedulersProvider: SchedulersProvider
    @Inject lateinit var repository: FarmdropRepository
    @Mock lateinit var requestStatusObserver: Observer<RequestStatus>
    @Mock lateinit var dataSourceStatusObserver: Observer<DataSourceStatus>

    private val testRoomDataSource = TestRoomDataSource()
    private val connectionStateMonitor = TestConnectionStateMonitor()

    private val testRepositoryComponent by lazy {
        DaggerTestRepositoryComponent.builder()
            .testProducersRepositoryModule(
                TestProducersRepositoryModule(
                    farmdropService,
                    producersDao,
                    connectionStateMonitor
                )
            )
            .testSchedulersModule(TestSchedulersModule())
            .build()
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        testRepositoryComponent.inject(this)
    }

    @Test
    fun testRepositoryCachesData() {
        mockSuccessApiCall()
        mockProducerDao()
        connectionStateMonitor.testNetworkAvailable = true
        repository.getProducersPagedList()
        verify(producersDao).insertProducers(anyList())
    }


    @Test
    fun testStatusesOnApiCall() {
        mockProducerDao()

        connectionStateMonitor.testNetworkAvailable = true

        mockExceptionOnApiCall()

        getPageWithSubscriptions()

        verify(requestStatusObserver).onChanged(RequestStatus.StatusFailed)
        verify(dataSourceStatusObserver).onChanged(DataSourceStatus.Cache)

        mockSuccessApiCall()

        repository.retry()
        verify(requestStatusObserver).onChanged(RequestStatus.StatusFinished)
        verify(dataSourceStatusObserver).onChanged(DataSourceStatus.Api)
    }

    @Test
    fun testStatusesOnUnavailableNetwork() {
        mockProducerDao()
        connectionStateMonitor.testNetworkAvailable = false

        getPageWithSubscriptions()

        verify(requestStatusObserver).onChanged(RequestStatus.StatusFailed)
        verify(dataSourceStatusObserver).onChanged(DataSourceStatus.Cache)
    }

    @Test
    fun testRepositoryLoadsDataOnConnectionAvailable() {
        mockProducerDao()
        connectionStateMonitor.testNetworkAvailable = false

        getPageWithSubscriptions()

        verify(requestStatusObserver).onChanged(RequestStatus.StatusFailed)
        verify(dataSourceStatusObserver).onChanged(DataSourceStatus.Cache)

        mockSuccessApiCall()
        connectionStateMonitor.testNetworkAvailable = true
        connectionStateMonitor.testNetworkAppeared()

        verify(requestStatusObserver).onChanged(RequestStatus.StatusInProgress)
        verify(requestStatusObserver).onChanged(RequestStatus.StatusFinished)
        verify(dataSourceStatusObserver).onChanged(DataSourceStatus.Api)
        verify(producersDao).insertProducers(anyList())
    }

    private fun getPageWithSubscriptions() {
        val page = repository.getProducersPagedList()
        page.requestStatus.observeForever(requestStatusObserver)
        page.dataSource.observeForever(dataSourceStatusObserver)
    }

    private fun mockSuccessApiCall() {
        `when`(farmdropService.getFarmdropProducers(any(Int::class.java), any(Int::class.java)))
            .thenReturn(Flowable.just(FarmdropProducersResponse()))
    }

    private fun mockExceptionOnApiCall() {
        `when`(farmdropService.getFarmdropProducers(any(Int::class.java), any(Int::class.java)))
            .thenReturn(Flowable.error(Exception()))
    }

    private fun mockProducerDao() {
        `when`(producersDao.selectPaged()).then { TestRoomFactory(testRoomDataSource) }
    }

}
