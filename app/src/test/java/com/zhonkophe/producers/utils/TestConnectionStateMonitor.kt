package com.zhonkophe.producers.utils

import com.zhonkophe.producers.util.connectivity.ConnectionStateMonitor
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject

 class TestConnectionStateMonitor : ConnectionStateMonitor {

    private val connectionAvailability = BehaviorSubject.create<Boolean>()

    var testNetworkAvailable: Boolean? = null

    fun testNetworkAppeared() {
        connectionAvailability.onNext(true)
    }

    fun testNetworkDisappeared() {
        connectionAvailability.onNext(false)
    }

    override fun connectionAvailability(): Flowable<Boolean> =
        connectionAvailability.toFlowable(BackpressureStrategy.LATEST)

    override fun isNetworkAvailable(): Boolean = testNetworkAvailable == true
}