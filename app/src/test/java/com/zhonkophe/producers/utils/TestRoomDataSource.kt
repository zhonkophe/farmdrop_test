package com.zhonkophe.producers.utils

import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.zhonkophe.producers.datasource.cache.RoomFarmdropProducer

class TestRoomFactory(private val dataSource: TestRoomDataSource) : DataSource.Factory<Int, RoomFarmdropProducer>() {
    override fun create(): DataSource<Int, RoomFarmdropProducer> = dataSource
}

class TestRoomDataSource : PageKeyedDataSource<Int, RoomFarmdropProducer>() {
    var initialList = emptyList<RoomFarmdropProducer>()
    var nextList = emptyList<RoomFarmdropProducer>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, RoomFarmdropProducer>) {
        callback.onResult(initialList, 1, 1)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, RoomFarmdropProducer>) {
        callback.onResult(nextList, 2)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, RoomFarmdropProducer>) {
        //nothing to do here
    }

}