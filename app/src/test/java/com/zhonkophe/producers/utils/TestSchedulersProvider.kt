package com.zhonkophe.producers.utils

import com.zhonkophe.producers.util.schedulers.SchedulersProvider
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class TestSchedulersProvider(
    override val backgroundScheduler: Scheduler = Schedulers.trampoline(),
    override val mainScheduler: Scheduler = Schedulers.trampoline(),
    override val singleScheduler: Scheduler = Schedulers.trampoline()
) : SchedulersProvider