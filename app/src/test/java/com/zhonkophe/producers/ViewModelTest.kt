package com.zhonkophe.producers

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.nhaarman.mockitokotlin2.verify
import com.zhonkophe.producers.di.DaggerTestViewComponent
import com.zhonkophe.producers.di.TestViewModelModule
import com.zhonkophe.producers.repository.*
import com.zhonkophe.producers.viewmodel.FarmdropProducersViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import javax.inject.Inject

@RunWith(JUnit4::class)
class ViewModelTest {

    @Rule @JvmField var mockitoRule = MockitoJUnit.rule()
    @Rule @JvmField var instantExecutorRule = InstantTaskExecutorRule()

    @Mock lateinit var repository: FarmdropRepository
    @Mock lateinit var requestStatusObserver: Observer<RequestStatus>
    @Mock lateinit var dataSourceStatusObserver: Observer<DataSourceStatus>
    @Mock lateinit var emptyStateObserver: Observer<Boolean>

    @Inject lateinit var viewModel: FarmdropProducersViewModel

    private val testRequestStatus = MutableLiveData<RequestStatus>()
    private val testDataSourceStatus = MutableLiveData<DataSourceStatus>()
    private val testPaging = MutableLiveData<PagedList<FarmdropProducer>>()

    private val testData = FarmdropProducersListing(testPaging, testRequestStatus, testDataSourceStatus)

    private val testViewModelComponent by lazy {
        DaggerTestViewComponent.builder()
            .testViewModelModule(TestViewModelModule(repository))
            .build()
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        testViewModelComponent.inject(this)
    }

    @Test
    fun testViewModelStart() {
        mockSuccessRepositoryData()
        viewModel.start()
        verify(repository).getProducersPagedList()
    }

    @Test
    fun testStatusChanges() {
        mockSuccessRepositoryData()
        viewModel.start()

        testRequestStatus.postValue(RequestStatus.StatusFinished)
        verify(requestStatusObserver).onChanged(RequestStatus.StatusFinished)

        testRequestStatus.postValue(RequestStatus.StatusFailed)
        verify(requestStatusObserver).onChanged(RequestStatus.StatusFailed)

        testRequestStatus.postValue(RequestStatus.StatusInProgress)
        verify(requestStatusObserver).onChanged(RequestStatus.StatusInProgress)

        testDataSourceStatus.postValue(DataSourceStatus.Api)
        verify(dataSourceStatusObserver).onChanged(DataSourceStatus.Api)

        testDataSourceStatus.postValue(DataSourceStatus.Cache)
        verify(dataSourceStatusObserver).onChanged(DataSourceStatus.Cache)
    }

    @Test
    fun testEmptyState() {
        mockSuccessRepositoryData()
        viewModel.nothingToShow.observeForever(emptyStateObserver)
        viewModel.start()
        viewModel.listingMediator.observeForever { }

        val emptyItems = emptyList<FarmdropProducer>()
        testPaging.postValue(mockPagedList(emptyItems))
        verify(emptyStateObserver).onChanged(true)

        val notEmptyItems = listOf(FarmdropProducer("test", "test", "test", "test"))
        testPaging.postValue(mockPagedList(notEmptyItems))
        verify(emptyStateObserver).onChanged(false)
    }

    @Test
    fun testRetry() {
        mockSuccessRepositoryData()
        viewModel.retry()
        verify(repository).retry()
    }

    @Test
    fun testRefresh() {
        mockSuccessRepositoryData()
        viewModel.refresh()
        verify(repository).reload()
    }

    private fun mockSuccessRepositoryData() {
        `when`(repository.getProducersPagedList()).thenReturn(testData)
        testData.requestStatus.observeForever(requestStatusObserver)
        testData.dataSource.observeForever(dataSourceStatusObserver)
    }

    private fun <T> mockPagedList(list: List<T>): PagedList<T> {
        val pagedList = Mockito.mock(PagedList::class.java) as PagedList<T>
        Mockito.`when`(pagedList[ArgumentMatchers.anyInt()]).then { invocation ->
            val index = invocation.arguments.first() as Int
            list[index]
        }
        Mockito.`when`(pagedList.size).thenReturn(list.size)
        Mockito.`when`(pagedList.isNullOrEmpty()).thenReturn(list.size == 0)
        return pagedList
    }
}